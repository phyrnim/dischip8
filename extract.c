unsigned int extract(unsigned int code, int mn, int ila){
    unsigned int e_code;
    unsigned int hexa;

    switch(mn){
    case 0 : hexa = 0xFFFF; break;
    case 1 : hexa = 0x7FFF; break;
    case 2 : hexa = 0x3FFF; break;
    case 3 : hexa = 0x1FFF; break;
    case 4 : hexa = 0x0FFF; break;
    case 5 : hexa = 0x07FF; break;
    case 6 : hexa = 0x03FF; break;
    case 7 : hexa = 0x01FF; break;
    case 8 : hexa = 0x00FF; break;
    case 9 : hexa = 0x007F; break;
    case 10 : hexa = 0x003F; break;
    case 11 : hexa = 0x001F; break;
    case 12 : hexa = 0x000F; break;
    case 13 : hexa = 0x0007; break;
    case 14 : hexa = 0x0003; break;
    case 15 : hexa = 0x0001; break;

    }

    e_code = (code & hexa) >> 15 - ila;
    return e_code;
}
