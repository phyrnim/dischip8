#include <stdio.h>
#include <stdlib.h>

#include "extract.c"

unsigned short memory[100000];

void disassemble(FILE *fp);

int main()
{
    char file_name[30];
    scanf("%s", &file_name);

    FILE *fp;

    fp = fopen(file_name, "rb");
    disassemble(fp);

}

void disassemble(FILE *fp)
{
    unsigned int byte;
    int byte_nb = 0;
    int PC = 0;
    unsigned int code;


    while ((byte = getc(fp)) != EOF) {
	memory[byte_nb] = byte;
	byte_nb++;
   }

    while (PC < byte_nb){
	code = (unsigned int)(memory[PC] << 8 | memory[PC+1 << 0]);

	
	unsigned int _byte1 = extract(code, 0, 3);
	unsigned int _byte2 = extract(code, 4, 7);
	unsigned int _byte3 = extract(code, 8, 11);
	unsigned int _byte4 = extract(code, 12, 15);

	if (code == 0x00E0) printf("%x : CLEAR\n", code);
	if (code == 0x00EE) printf("%x : RETURN\n", code);
	if (_byte1 == 0x1)
	{
	    printf("%x : JMP %x\n", code, extract(code, 4, 15));
	}

	if (_byte1 == 0x2)
	{
	    printf("%x : CALL %x\n", code, extract(code, 4, 15));
	}

	if (_byte1 == 0x3)
	{
	    printf("%x : SKIP IF R%x == %x\n", code, _byte2, extract(code, 8, 15));
	}

	if (_byte1 == 0x4)
	{
	    printf("%x : SKIP IF R%x != %x\n", code, _byte2, extract(code, 8, 15));
	}

	if (_byte1 == 0x5)
	{
	    printf("%x : SKIP IF R%x == %x\n", code, _byte2, _byte3);
	}

	if (_byte1 == 0x6)
	{
	    printf("%x : MOV R%x, %x\n", code, _byte2, extract(code, 8, 15));
	}

	if (_byte1 == 0x7)
	{
	    printf("%x : ADD R%x, %x\n", code, _byte2, extract(code, 8,15));
	}

	if (_byte1 == 0x8)
	{
	    if (_byte4 == 0x0)
	    {
		printf("%x : MOV R%x, %x\n", code, _byte2, _byte3);
	    }
	    if (_byte4== 0x1)
	    {
		printf("%x : MOV R%x, R%x OR R%x\n", code, _byte2, _byte2, _byte3);
	    }
	    if (_byte4 == 0x2)
	    {
		printf("%x : MOV R%x, R%x AND R%x\n", code, _byte2, _byte2, _byte3);
	    }
	    if (_byte4 == 0x3)
	    {
		printf("%x : MOV R%x, R%x XOR R%x\n", code, _byte2, _byte2, _byte3);
	    }
	    if (_byte4 == 0x4)
	    {
		printf("%x : ADD R%x, R%x\n", code, _byte2, _byte3);
	    }
	    if (_byte4 == 0x5)
	    {
		printf("%x : SUB R%x, R%x\n", code, _byte2, _byte3);
	    }
	    if (_byte4 == 0x6)
	    {
		printf("%x : R%x>>1 ; MOV R%x, R%x\n", code, _byte3, _byte2, _byte3);
	    }
	    if (_byte4 == 0x7)
	    {
		printf("%x : MOV R%x, (R%x - R%x)\n", code, _byte2, _byte3, _byte2);
	    }
	    if (_byte4 == 0xE)
	    {
		printf("%x : R%x<<1 ; MOV R%x, R%x\n", code, _byte3, _byte2, _byte3);
	    }
	}

	if (_byte1 == 0x9)
	{
	    printf("%x : SKIP IF R%x != R%x\n", code, _byte2, _byte3);
	}

	if (_byte1 == 0xA)
	{
	    printf("%x : MOV I, %x\n", code, extract(code, 4, 15));
	}

	if (_byte1 == 0xB)
	{
	    printf("%x : JMP %x + R0\n", code, extract(code, 4,15));
	}

	if (_byte1 == 0xC)
	{
	    printf("%x : MOV R%x, RAND & %x\n", code, _byte2, extract(code, 8,15));
	}

	if (_byte1 == 0xD)
	{
	    printf("%x : DRAW R%x, R%x, %x\n", code, _byte2, _byte3, _byte4);
	}

	if (_byte1 == 0xE)
	{
	    if (_byte4 == 0xE)
	    {
		printf("%x : SKIP IF [KEY] == R%x\n", code, _byte2);
	    }

	    if (_byte4 == 0x1)
	    {
		printf("%x : SKIP IF [KEY] != R%x\n", code, _byte2);
	    }
	}

	if (_byte1 == 0xF)
	{
	    //à continuer
	}
	
	PC += 2;	
    }

}
